"""https://adventofcode.com/2019/day/6"""

import collections


def solveA(M):
    answer = {}
    def dfs(n):
        if n not in M:
            return 0
        if n not in answer:
            answer[n] = len(M[n]) + sum(dfs(n2) for n2 in M[n])
        return answer[n]
    return sum(dfs(n) for n in M.keys())

def solve_me(d: dict, n):
    seen = {}
    if n not in d:
        return 0
    return len(d[n]) + solve_me(d, d[n][0])

def solve(M):
    answer = {}
    def dfs(n):
        if n not in M:
            return 0  # Not a key
        if n not in answer:
            for val in M[n]:
                return len(M[n]) + sum(dfs(n2) for n2 in M[n])
        return answer[n]
    t = 0
    for n1 in M.keys():
        t += dfs(n1)
    return(t)


def solveB(M):
    # make undirected
    M2 = collections.defaultdict(set)
    for a, vs in M.items():
        for b in vs:
            M2[a].add(b)
            M2[b].add(a)
    M = M2

    seen = set()
    def dfs(n):
        if n == 'SAN':
            return -2
        if n in seen:
            return None
        seen.add(n)
        for n2 in M[n]:
            dist = dfs(n2)
            if dist is not None:
                return dist + 1
        return None
    return dfs('YOU')


if __name__ == '__main__':
    with open("input.txt") as f:
        d = collections.defaultdict(set)
        for line in f:
            a, b = line.strip().split(')')
            if a in d:
                d[a].add(b)
            else:
                d[a] = set([b])
        # print(d)
        # print(solve(d))
        # print(solveB(d))
    # graph = {'A': set(['B', 'C']),
    #          'B': set(['A', 'D', 'E']),
    #          'C': set(['A', 'F']),
    #          'D': set(['B']),
    #          'E': set(['B', 'F']),
    #          'F': set(['C', 'E'])}


    def dfs(graph, start):
        visited, stack = set(), [start]
        while stack:
            vertex = stack.pop()
            if vertex not in visited:
                visited.add(vertex)
                stack.extend(graph[vertex] - visited)
        return visited


    print(dfs(d, 'COM'))  # {'E', 'D', 'F', 'A', 'C', 'B'}
    print(len(dfs(d, "COM")))