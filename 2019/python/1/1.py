def _rec_fuel(_fuel):
    fuel = int(int(_fuel)/3) - 2
    if fuel <= 0:
        return 0
    return fuel + _rec_fuel(fuel)


if __name__ == '__main__':
    total = 0
    with open("file.txt") as f:
        for x in f:
            total += _rec_fuel(int(x))
    print(total)

    assert total == 50346
