"""https://adventofcode.com/2019/day/3"""


def fill_grid(row: str):
    """Consider a list of points"""
    values = []
    x, y = 0, 0
    for k in row.split(","):
        if k[0] == "R":
            for a in range(int(k[1:])):
                x += 1
                values.append(f"{x},{y}")
        elif k[0] == "U":
            for b in range(int(k[1:])):
                y += 1
                values.append(f"{x},{y}")
        elif k[0] == "L":
            for c in range(int(k[1:])):
                x -= 1
                values.append(f"{x},{y}")
        elif k[0] == "D":
            for d in range(int(k[1:])):
                y -= 1
                values.append(f"{x},{y}")
    return values


def get_distance(coords: set):
    m = float("inf")
    for coord in coords:
        m = min(m, abs(int(coord[0:coord.find(",")])) + abs(int(coord[coord.find(",")+1:])))
    return m


if __name__ == '__main__':
    with open("input_main.txt") as f:
        for i, row in enumerate(f):
            if i == 0:
                vals1 = fill_grid(row)
            if i == 1:
                vals2 = fill_grid(row)

    print(vals1)
    print(vals2)
    print(set(vals1).intersection(vals2))
    print(get_distance(set(vals1).intersection(vals2)))
