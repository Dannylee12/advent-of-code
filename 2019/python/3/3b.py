"""https://adventofcode.com/2019/day/3"""
steps = [{}, {}]


def fill_grid(row: str, r: int):
    """Consider a list of points"""
    values = []
    x, y, s = 0, 0, 0
    for k in row.split(","):
        if k[0] == "R":
            for a in range(int(k[1:])):
                x += 1
                s += 1
                values.append(f"{x},{y}")
                steps[r][f"{x},{y}"] = s
        elif k[0] == "U":
            for b in range(int(k[1:])):
                y += 1
                s += 1
                values.append(f"{x},{y}")
                steps[r][f"{x},{y}"] = s
        elif k[0] == "L":
            for c in range(int(k[1:])):
                x -= 1
                s += 1
                values.append(f"{x},{y}")
                steps[r][f"{x},{y}"] = s
        elif k[0] == "D":
            for d in range(int(k[1:])):
                y -= 1
                s += 1
                values.append(f"{x},{y}")
                steps[r][f"{x},{y}"] = s

    print(steps)
    return values


def get_distance(coords: set):
    m = float("inf")
    for coord in coords:
        m = min(m, abs(int(coord[0:coord.find(",")])) + abs(int(coord[coord.find(",")+1:])))
    return m


def get_steps(coords: set):
    s = float("inf")
    global steps
    for coord in coords:
        s = min(steps[0][coord] + steps[1][coord], s)
    return s


if __name__ == '__main__':
    with open("input_main.txt") as f:
        for i, row in enumerate(f):
            if i == 0:
                vals1 = fill_grid(row, 0)
            if i == 1:
                vals2 = fill_grid(row, 1)

    print(get_steps(set(vals1).intersection(vals2)))
