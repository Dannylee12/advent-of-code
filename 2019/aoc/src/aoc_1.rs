use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn fuel_required(input: i32) -> i32 {
    (input / 3) - 2
}

fn fuel_required_rec(input: i32) -> i32 {
    let mut fuel_calc = (input / 3) - 2;
    if fuel_calc < 1 {
        return 0
    }
    fuel_calc + fuel_required_rec(fuel_calc)
}


pub fn a() {
    assert_eq!(fuel_required(14), 2);
    assert_eq!(fuel_required(12), 2);
    assert_eq!(fuel_required(1969), 654);
    assert_eq!(fuel_required(100756), 33583);
    let mut total_fuel = 0;
    let file = File::open("src/input.txt").expect("Unable to Open file");
    let reader = BufReader::new(file);
    for line in reader.lines() {
        for word in line.unwrap().split_whitespace(){
            total_fuel += fuel_required(word.parse().unwrap());
        }
    }
    println!("{}", total_fuel);
}

pub fn b() {
    assert_eq!(fuel_required_rec(14), 2);
    assert_eq!(fuel_required_rec(12), 2);
    assert_eq!(fuel_required_rec(1969), 966);
    assert_eq!(fuel_required_rec(100756), 50346);
    let mut total_fuel = 0;
    let file = File::open("src/input.txt").expect("Unablet to Open file");
    let reader = BufReader::new(file);
    for line in reader.lines() {
        for word in line.unwrap().split_whitespace(){
            total_fuel += fuel_required_rec(word.parse().unwrap());
        }
    }
    println!("{}", total_fuel);
}