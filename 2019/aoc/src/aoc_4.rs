
pub fn pass(num: i32) -> bool {
    // 6 digit number
    if num < 99999 {
        return false
    }
    // At least two duplicates
    let mut valid = false;
    let mut prev_value = 'a';
    let value:char;
    let num_str = num.to_string();
    for i in num_str.chars() {
        if prev_value == 'a' {
            prev_value = i;
        }
        else if (i as i32) < (prev_value as i32) {
            return false;
        }
        else if i == prev_value {
            valid = true;
        }
        prev_value = i;
    }
    valid
}

pub fn duplicates(num: i32) -> bool {
    let str_num = num.to_string();
    let mut valid = false;
    for x in str_num.chars() {
        if str_num.matches(x).count() == 2 {
            valid = true;
        }
    }
    valid
}

pub fn main_4(start: i32, end: i32) -> i32 {
    let mut total = 0;
    for i in start..end {
        if pass(i) && duplicates(i) {
            println!("{}", i);
            total += 1
        }
    }
    println!("{}", total);
    total
}